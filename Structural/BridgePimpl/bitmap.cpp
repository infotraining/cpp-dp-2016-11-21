#include "bitmap.hpp"
#include <iostream>
#include <algorithm>

struct Bitmap::BitmapImpl
{
    std::vector<char> image_;
};

Bitmap::Bitmap(const std::__cxx11::string& path)
    : impl_{std::make_unique<BitmapImpl>()}
{
    impl_->image_.assign(path.begin(), path.end());
}

Bitmap::~Bitmap() = default;

void Bitmap::draw() const
{
    for(const auto& pixel : impl_->image_)
        std::cout << pixel;
    std::cout << std::endl;
}
