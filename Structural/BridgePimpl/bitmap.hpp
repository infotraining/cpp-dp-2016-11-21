#ifndef BITMAP_HPP
#define BITMAP_HPP

#include <string>
#include <memory>

class Bitmap
{
    class BitmapImpl;
    std::unique_ptr<BitmapImpl> impl_;
public:
    Bitmap(const std::string& path);
    ~Bitmap();
    void draw() const;
};

#endif // BITMAP_HPP
