#include "text_reader_writer.hpp"
#include "../shape_factories.hpp"
#include "../text.hpp"

namespace
{
    using namespace Drawing;
    using namespace Drawing::IO;

    bool is_registered =
            SingletonShapeRWFactory::instance()
                .register_creator(std::type_index{typeid(Text)}, &std::make_unique<TextReaderWriter>);

}

void Drawing::IO::TextReaderWriter::read(Drawing::Shape& shp, std::istream& in)
{
    Text& text = static_cast<Text&>(shp);

    Point pt;
    std::string str;
    in >> pt >> str;

    text.set_coord(pt);
    text.set_text(str);
}

void Drawing::IO::TextReaderWriter::write(Drawing::Shape& shp, std::ostream& out)
{
    Text& text = static_cast<Text&>(shp);

    out << Text::id << " " << text.coord() << " " << text.text() << std::endl;
}
