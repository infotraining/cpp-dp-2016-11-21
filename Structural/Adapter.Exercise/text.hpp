#ifndef TEXT_HPP
#define TEXT_HPP

#include <string>
#include "shape.hpp"
#include "paragraph.hpp"

namespace Drawing
{

    // TODO - zaadaptowac klase Paragraph do wymogow klienta
    class Text : public ShapeBase, private LegacyCode::Paragraph
    {
    public:
        static constexpr const char* id = "Text";

        Text(int x = 0, int y = 0, const std::string& text = "")
            : ShapeBase{x, y}, LegacyCode::Paragraph(text.c_str())
        {}

        std::string text() const
        {
            return get_paragraph();
        }

        void set_text(const std::string& text)
        {
            set_paragraph(text.c_str());
        }

        void draw() const override
        {
            render_at(coord().x, coord().y);
        }
    };

}

#endif
