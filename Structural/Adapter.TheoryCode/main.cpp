#include <iostream>
#include "adapter.hpp"

using namespace std;

int main()
{
	Client client;

	cout << "-- do_operation on ClassAdapter" << endl;
	ClassAdapter cadapter;
	client.do_operation(cadapter);

	cout << endl;

	cout << "-- do_operation on ObjectAdapter" << endl;
	Adaptee adaptee;
	ObjectAdapter oadapter(adaptee);
	client.do_operation(oadapter);
}
