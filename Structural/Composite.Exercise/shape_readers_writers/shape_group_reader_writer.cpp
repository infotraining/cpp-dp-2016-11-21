#include "shape_group_reader_writer.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

namespace
{
    bool is_registered = SingletonShapeRWFactory::instance().register_creator(
                type_index(typeid(ShapeGroup)),
                []{ return std::make_unique<ShapeGroupReaderWriter>(SingletonShapeFactory::instance(), SingletonShapeRWFactory::instance()); });
}

void ShapeGroupReaderWriter::read(Shape& shp, std::istream& in)
{
    ShapeGroup& shape_group = static_cast<ShapeGroup&>(shp);

    int size;

    in >> size;

    for(int i = 0; i < size; ++i)
    {
        std::string shape_id;
        in >> shape_id;
        std::cout << "Loading in ShapeGroup: " << shape_id << "..." << std::endl;

        auto shape = shape_factory_.create(shape_id);
        auto shape_rw = shape_rw_factory_.create(type_index(typeid(*shape)));

        shape_rw->read(*shape, in);


        shape_group.add(move(shape));
    }
}
