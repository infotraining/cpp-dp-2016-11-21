#include "../shape_factories.hpp"
#include "text_reader_writer.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

namespace
{
    bool is_registered = SingletonShapeRWFactory::instance().register_creator(
                type_index(typeid(Text)),
                []{ return make_unique<Drawing::IO::TextReaderWriter>(); });
}

void Drawing::IO::TextReaderWriter::read(Drawing::Shape& shp, std::istream& in)
{
    Point pt;
    std::string str;

    in >> pt >> str;

    Text& text = static_cast<Text&>(shp);
    text.set_coord(pt);
    text.set_text(str);
}
