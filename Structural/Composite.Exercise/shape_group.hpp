#ifndef SHAPEGROUP_HPP
#define SHAPEGROUP_HPP

#include <vector>
#include <memory>

#include "shape.hpp"

namespace Drawing
{
    using ShapePtr = std::unique_ptr<Shape>;

    class ShapeGroup : public CloneableShape<ShapeGroup>
    {
        std::vector<ShapePtr> shapes_;
    public:
        static constexpr const char* id = "ShapeGroup";

        ShapeGroup() = default;

        ShapeGroup(const ShapeGroup& source)
        {
            shapes_.reserve(source.shapes_.size());
            for(const auto& shp : source.shapes_)
                shapes_.push_back(shp->clone());
        }

        Shape& operator=(const ShapeGroup& source)
        {
            ShapeGroup temp(source);
            swap(temp);

            return *this;
        }

        ShapeGroup(ShapeGroup&&) = default;
        ShapeGroup& operator=(ShapeGroup&&) = default;

        void swap(ShapeGroup& other)
        {
            shapes_.swap(other.shapes_);
        }

        void add(ShapePtr shape)
        {
            shapes_.push_back(std::move(shape));
        }

        void draw() const
        {
            for(const auto& shp : shapes_)
                shp->draw();
        }

        void move(int dx, int dy)
        {
            for(const auto& shp : shapes_)
                shp->move(dx, dy);
        }
    };

}


#endif // SHAPEGROUP_HPP
