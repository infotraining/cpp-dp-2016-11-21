#include <algorithm>
#include "shape_group.hpp"
#include "shape_factories.hpp"

using namespace std;
using namespace Drawing;

namespace
{
    bool is_registered =
            SingletonShapeFactory::instance()
                .register_creator(ShapeGroup::id, &make_unique<ShapeGroup>);
}
