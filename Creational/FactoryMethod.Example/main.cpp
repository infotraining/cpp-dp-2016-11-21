#include <iostream>
#include <vector>
#include <memory>
#include <cassert>
#include <fstream>
#include <unordered_map>
#include <typeindex>

#include "shape.hpp"
#include "rectangle.hpp"
#include "square.hpp"
#include "shape_readers_writers/rectangle_reader_writer.hpp"
#include "shape_readers_writers/square_reader_writer.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

using ShapeCreator = std::function<std::unique_ptr<Shape>()>;


template <
        typename ProductType,
        typename IndexType = std::string,
        typename CreatorType = std::function<std::unique_ptr<ProductType>()>>
class Factory
{
    std::unordered_map<IndexType, CreatorType> creators_;
public:
    bool register_creator(const IndexType& id, CreatorType creator)
    {
        return creators_.insert(std::make_pair(id, creator)).second;
    }

    std::unique_ptr<ProductType> create(const IndexType& id)
    {
        auto& creator = creators_.at(id);
        return creator();
    }
};

using ShapeFactory = Factory<Shape>;
using ShapeRWFactory = Factory<ShapeReaderWriter, type_index>;

class GraphicsDoc
{
    ShapeFactory& shape_factory_;
    ShapeRWFactory& shape_rw_factory_;
    vector<unique_ptr<Shape>> shapes_;
public:
    GraphicsDoc(ShapeFactory& sf, ShapeRWFactory& srwf)
        : shape_factory_{sf}, shape_rw_factory_{srwf}
    {}

    void add(unique_ptr<Shape> shp)
    {
        shapes_.push_back(move(shp));
    }

    void render()
    {
        for(const auto& shp : shapes_)
            shp->draw();
    }

    void load(const string& filename)
    {
        ifstream file_in{filename};

        if (!file_in)
        {
            cout << "File not found!" << endl;
            exit(1);
        }

        while (file_in)
        {
            string shape_id;
            file_in >> shape_id;        

            if (!file_in)
                return;

            cout << "Loading " << shape_id << "..." << endl;

            auto shape = shape_factory_.create(shape_id);
            auto shape_rw = shape_rw_factory_.create(type_index{typeid(*shape)});

            shape_rw->read(*shape, file_in);

            shapes_.push_back(move(shape));
        }
    }

    void save(const string& filename)
    {
        ofstream file_out{filename};

        for(const auto& shp : shapes_)
        {
            auto shape_rw = shape_rw_factory_.create(type_index{typeid(*shp)});
            shape_rw->write(*shp, file_out);
        }
    }
};

void bootstrap(ShapeFactory& sf)
{

    sf.register_creator(Square::id, [] { return std::make_unique<Square>();});
}

ShapeFactory shape_factory;

void bootstrap(ShapeRWFactory& srwf)
{
    srwf.register_creator(type_index{typeid(Rectangle)}, &make_unique<RectangleReaderWriter>);
    srwf.register_creator(type_index{typeid(Square)}, &make_unique<SquareReaderWriter>);
}

int main()
{

    bootstrap(shape_factory);

    ShapeRWFactory shape_rw_factory;
    bootstrap(shape_rw_factory);

    cout << "Start..." << endl;

    GraphicsDoc doc{shape_factory, shape_rw_factory};

    doc.load("drawing.txt");

    cout << "\n";

    doc.render();

    doc.save("new_drawing.txt");
}
