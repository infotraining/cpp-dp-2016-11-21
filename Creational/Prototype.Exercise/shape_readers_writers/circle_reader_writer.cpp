#include "circle_reader_writer.hpp"
#include "../shape_factories.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

namespace
{
    bool is_registered
        = SingtonShapeRWFactory::instance().register_creator(
                type_index(typeid(Circle)), make_unique<CircleReaderWriter>);
}


void CircleReaderWriter::read(Shape& shp, std::istream& in)
{
    Point pt;
    int r;

    in >> pt >> r;

    Circle& c = static_cast<Circle&>(shp);
    c.set_coord(pt);
    c.set_radius(r);
}
