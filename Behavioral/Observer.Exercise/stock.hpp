#ifndef STOCK_HPP_
#define STOCK_HPP_

#include <string>
#include <iostream>
#include <boost/signals2.hpp>

// Subject
class Stock
{
private:
	std::string symbol_;
	double price_;
    using PriceChangeEvent = boost::signals2::signal<void(const std::string&, double)>;
    using PriceChangedCallback = PriceChangeEvent::slot_type;

    PriceChangeEvent price_changed_; // signal
public:
	Stock(const std::string& symbol, double price) : symbol_(symbol), price_(price)
	{
	}

	std::string get_symbol() const
	{
		return symbol_;
	}

	double get_price() const
	{
		return price_;
	}

    boost::signals2::connection register_callback(PriceChangedCallback callback)
    {
        return price_changed_.connect(callback);
    }

	void set_price(double price)
	{
        if (price != price_)
        {
            price_ = price;
            price_changed_(symbol_, price_); // notification
        }
	}
};

class Investor
{
	std::string name_;
public:
	Investor(const std::string& name) : name_(name)
	{
	}

    void update_portfolio(const std::string& symbol, double price)
	{
        std::cout << name_ << " updates his portfolio - " << symbol << " - " << price << "$" << std::endl;
	}
};

#endif /*STOCK_HPP_*/
