#include "stock.hpp"

using namespace std;

int main()
{
	Stock misys("Misys", 340.0);
	Stock ibm("IBM", 245.0);
	Stock tpsa("TPSA", 95.0);

    Investor kulczyk_jr{"Kulczyk Jr"};
    Investor czarnecki{"Stefek"};

    misys.register_callback([&kulczyk_jr](const auto& symbol, auto price)
                            { kulczyk_jr.update_portfolio(symbol, price);});
    ibm.register_callback([&kulczyk_jr](const auto& symbol, auto price) { kulczyk_jr.update_portfolio(symbol, price);});
    auto conn1 = misys.register_callback([&czarnecki](const auto& symbol, auto price) { czarnecki.update_portfolio(symbol, price);});

	// zmian kurs�w
	misys.set_price(360.0);
	ibm.set_price(210.0);
	tpsa.set_price(45.0);

    cout << "\n\n";
    conn1.disconnect();

	misys.set_price(380.0);
	ibm.set_price(230.0);
	tpsa.set_price(15.0);
}
