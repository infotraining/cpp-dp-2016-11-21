#include <iostream>

class TurnstileApi;
class TurnstileFSM;
class LockedState;
class UnlockedState;

class TurnstileState
{
public:
    virtual TurnstileState* Coin(TurnstileApi&) = 0;
    virtual TurnstileState* Pass(TurnstileApi&) = 0;
	virtual ~TurnstileState() {}
};

class UnlockedState : public TurnstileState
{
public:
    TurnstileState* Coin(TurnstileApi& t);
    TurnstileState* Pass(TurnstileApi& t);
};

class LockedState : public TurnstileState
{
public:
    TurnstileState* Coin(TurnstileApi& t);
    TurnstileState* Pass(TurnstileApi& t);
};

class TurnstileApi
{
public:
	virtual void Lock() { std::cout << "Lock" << std::endl; }
	virtual void Unlock() { std::cout << "Unlock" << std::endl; };
	virtual void Thankyou() { std::cout << "Thank You" << std::endl; };
	virtual void Alarm() { std::cout << "Alarm" << std::endl; };
    virtual ~TurnstileApi() = default;
};

class TurnstileFSM
{
    TurnstileApi turnstile_;
public:
	TurnstileFSM() : itsState(&lockedState)
	{
	}

    void Coin() { itsState = itsState->Coin(turnstile_); }
    void Pass() { itsState = itsState->Pass(turnstile_); }
	
	static LockedState lockedState;
	static UnlockedState unlockedState;

private:
	TurnstileState* itsState;
};

LockedState TurnstileFSM::lockedState;
UnlockedState TurnstileFSM::unlockedState;

TurnstileState* UnlockedState::Coin(TurnstileApi& t)
{
    t.Thankyou();
	return this;
}

TurnstileState* UnlockedState::Pass(TurnstileApi& t)
{
    t.Lock();
	return &TurnstileFSM::lockedState;
}

TurnstileState* LockedState::Coin(TurnstileApi& t)
{
    t.Unlock();
	return &TurnstileFSM::unlockedState;
}

TurnstileState* LockedState::Pass(TurnstileApi& t)
{
    t.Alarm();
	return this;
}

int main()
{
	TurnstileFSM turnstile;

	turnstile.Coin();
	turnstile.Pass();
	turnstile.Pass();
	turnstile.Coin();
	turnstile.Coin();
    turnstile.Pass();
    turnstile.Pass();
    turnstile.Coin();
    turnstile.Coin();
    turnstile.Coin();
    turnstile.Coin();

}
