#ifndef STATE_HPP_
#define STATE_HPP_

#include <iostream>
#include <string>
#include <memory>
#include <typeinfo>


class Context;

// "State"



// "Context"
class Context
{
    class State
    {
    public:
        virtual void handle(Context& context) = 0;
        virtual ~State() {}
    };

    // "ConcreteStateA"
    class ConcreteStateA : public State
    {
    public:
        void handle(Context& context);
    };


    // "ConcreteStateB"
    class ConcreteStateB : public State
    {
    public:
        void handle(Context& context);
    };

    std::unique_ptr<State> state_;

    void set_state(std::unique_ptr<State> new_state)
    {
        state_ = std::move(new_state);
    }
public:
    Context() : state_{std::make_unique<ConcreteStateA>()}
	{
	}

	Context(const Context&) = delete;
	Context& operator=(const Context&) = delete;

	void request()
	{
		state_->handle(*this);
	}
};


void Context::ConcreteStateA::handle(Context& context)
{
	std::cout << "Context works in ConcreteStateA" << std::endl;

    context.set_state(std::make_unique<ConcreteStateB>());
}

void Context::ConcreteStateB::handle(Context& context)
{
	std::cout << "Context works in ConcreteStateB" << std::endl;

    context.set_state(std::make_unique<ConcreteStateA>());
}

#endif /*STATE_HPP_*/
