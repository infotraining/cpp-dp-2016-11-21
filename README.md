# Design Patterns in C++ #

## Dokumentacja

* [Dokumentacja](https://infotraining.bitbucket.io/cpp-dp/)

## Ustawienia proxy

We can add them to `.profile`

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```

## Ćwiczenia

### FactoryMethod.Exercise1

Zrefaktoryzować kod wykorzystując Factory Method. Zastąpić funkcję ``gen_info()`` wywołaniem metody wytwórczej.


### FactoryMethod.Exercise2

Dodać nowy kształt typu ``Circle`` do hierarchii:

1. Zaimplementować klasę Circle
2. Zarejestrować wytwórcę dla ``Circle`` w fabryce ``SingletonShapeFactory``
3. Zaimplementować klasę ``CircleReaderWriter``
4. Zarejestrować wytwórcę ``CircleReaderWriter`` dla w fabryce ``SingletonShapeRWFactory``


### Prototype.Exercise

Zaimplementować konstruktor kopiujący w klasie ``GraphicsDoc``. Dodać metodę ``clone()`` do interfejsu ``Shape`` i zaimplementować klonowanie w konkretnych klasach pochodnych.

### Adapter.Exercise

Zaadaptować klasę ``LegacyCode::Paragraph`` do oczekiwać klienta (tj. do interfejsu ``Shape``). Wykorzystać do implementacji adapter klasowy.

### Decorator.Exercise

Napisać klasę dekoratora dla kawy. Zaimplementować konkretne dekoratory: ``WhippedCream``, ``Whisky`` oraz ``ExtraEspresso``.

### Composite.Exercise

Dodać kompozyt ``ShapeGroup`` do projektu. 

### Strategy.Exercise

Zrefaktoryzować kod klasy DataAnalyzer wykorzystując wzorzec Strategy. Po refaktoryzacji kod powinien spełniać zasadę Open/Close

### State.Exercise

Zrefaktoryzować kod klasy ``BankAccount`` wykorzystując wzorzec ``State``.